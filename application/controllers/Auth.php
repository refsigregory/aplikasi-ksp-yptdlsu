<?php
defined('BASEPATH') OR exit('Access Denied!');

class Auth extends CI_Controller {
	public function __construct ()
	{
		parent::__construct();
		$this->load->model('auth_model');
		$this->load->model('anggota_model');
	}
    public function index()
	{
        header("Location: auth/login");
    }

	public function login()
	{
            $data['title'] = "Masuk";
            $data['login'] = true;
			$this->load->view('_templates/header', $data);
			$this->load->view('pages/login', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function checkLogin()
	{
		// cek login
		$this->auth_model->logged_in();
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		// memanggil fungsi cek login
		$this->auth_model->login($username, $password);
	}
	public function register()
	{
            $data['title'] = "Mendaftar";
            $data['login'] = true;
			$this->load->view('_templates/header', $data);
			$data['berkas'] = $this->anggota_model->getAllBerkas();
			$this->load->view('pages/register', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function aktivasi()
	{
            $data['title'] = "Aktivasi Akun";
            $data['login'] = true;
			$this->load->view('_templates/header', $data);
			$data['berkas'] = $this->anggota_model->getAllBerkas();
			$this->load->view('pages/aktivasi', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function processRegister(){
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["tipe_user"=> "anggota","verifikasi"=>"belum","status_anggota"=>"menunggu"]);
				$query = $this->db->insert("pengguna", $data);
				$id_anggota = $this->db->insert_id();
				$berkas = $this->anggota_model->getAllBerkas();
				$index = 1;
				foreach ($berkas as $row):
				
					if($_FILES['berkas-'.$row->id_berkas]):
						if ( ! $this->upload->do_upload('berkas-'.$row->id_berkas))
						{
								$err[] = $this->upload->display_errors();
						}
						else
						{
							$udata = array('upload_data' => $this->upload->data());
							$file_berkas[] = $udata['upload_data']['file_name'];
							$this->anggota_model->insertBerkas(["id_berkas" => $row->id_berkas, "id_anggota" => $id_anggota, "nama_file" => $udata['upload_data']['file_name']]);
						}
					endif;
					$index++;
				endforeach;
				$msg = "Berhasil Mendaftar";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('auth/login', 'refresh');
			} else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('auth/register', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('auth/register', 'refresh');
		}
	}

	public function processAktivasi(){
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["tipe_user"=> "anggota","verifikasi"=>"belum","status_anggota"=>"menunggu"]);
				//$query = $this->db->insert("pengguna", $data);
				$id_anggota = $this->session->userdata('id');
				$berkas = $this->anggota_model->getAllBerkas();
				$index = 1;
				foreach ($berkas as $row):
				
					if($_FILES['berkas-'.$row->id_berkas]):
						if ( ! $this->upload->do_upload('berkas-'.$row->id_berkas))
						{
								$err[] = $this->upload->display_errors();
						}
						else
						{
							$udata = array('upload_data' => $this->upload->data());
							$file_berkas[] = $udata['upload_data']['file_name'];
							$this->anggota_model->insertBerkas(["id_berkas" => $row->id_berkas, "id_anggota" => $id_anggota, "nama_file" => $udata['upload_data']['file_name']]);
						}
					endif;
					$index++;
				endforeach;
				$this->anggota_model->update($id_anggota,$data);
				$msg = "Berhasil Mengirim Berkas, Pengurus akan mengkonfirmasi data Anda.";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home', 'refresh');
			} else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('auth/register', 'refresh');
		}
	}

	public function logout()
	{
		// FUnsi Logout
		$this->session->sess_destroy();
		redirect('auth/login');
	}

}

