<?php
defined('BASEPATH') OR exit('Access Denied!');

require './vendor/autoload.php';
class Home extends CI_Controller {
	public function __construct ()
	{
		parent::__construct();
		$this->load->model('auth_model');
		$this->load->model('anggota_model');
		$this->load->model('transaksi_model');
		$this->auth_model->check();

		//if($this->session->userdata("type") == "admin")
		//{
		//	$this->auth_model->verifikasi_anggota();
		//}

		if($this->session->userdata("type") == "anggota")
		{
			print_r($this->anggota_model->getByID($this->session->userdata("id"))[0]->status_anggota);
		}

		
	}
	public function index()
	{
            $data['title'] = "Beranda";
            $data['page_title'] = "Beranda";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['notif'] = $this->anggota_model->getNotifByUser($this->session->userdata('id'));
			$this->load->view('pages/beranda', $data);
			$this->load->view('_templates/footer', $data);
	}
	
	public function pengguna()
	{
            $data['title'] = "Pengguna";
            $data['page_title'] = "Pengguna";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			$data['pengguna'] = $this->anggota_model->getAllPengguna();

			$this->load->view('pages/pengguna', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambah_pengguna()
	{
            $data['title'] = "Tambah Pengguna";
            $data['page_title'] = "Tambah Pengguna";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

		
			$this->load->view('pages/tambah_pengguna', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambahPengguna()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);


		/*if($_FILE['bukti_pembayaran']):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
		endif;*/

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota"=> "aktif"], ["verifikasi"=> "sudah"]);
				$this->anggota_model->insert($data);
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pengguna', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pengguna', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pengguna', 'refresh');
		}
	}

	
	public function ubah_pengguna()
	{
            $data['title'] = "Ubah Pengguna";
            $data['page_title'] = "Ubah Pengguna";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

		
			$this->load->view('pages/ubah_pengguna', $data);
			$this->load->view('_templates/footer', $data);
	}
	
	public function ubahPengguna()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);


		/*if($_FILE['bukti_pembayaran']):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
		endif;*/
		$id=$this->input->post('id_user');
		$data = $this->input->post();
		if(true) {
			unset($data['id_user']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["status_anggota"=> "aktif"], ["verifikasi"=> "sudah"]);
				$this->anggota_model->update($id,$data);
				$msg = "Data Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pengguna', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pengguna', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pengguna', 'refresh');
		}
	}
    
    public function anggota()
	{
            $data['title'] = "Anggota";
            $data['page_title'] = "Anggota";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			if(isset($_GET['aktif']))
			{
				$data = ["status_anggota" => "aktif"];
				$this->anggota_model->update( $this->input->get("aktif"), $data);
				$this->anggota_model->insertNotif($this->input->get("aktif"), "Pendaftaran Anda Telah Diterima");
				$msg = "Pendaftaran Anggota Berhasil Dikonfirmasi";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/anggota?', 'refresh');
			}

			$data['anggota'] = $this->anggota_model->getAll();

			$this->load->view('pages/anggota', $data);
			$this->load->view('_templates/footer', $data);
	}
	
	
	public function tambah_anggota()
	{
            $data['title'] = "Tambah Anggota";
            $data['page_title'] = "Tambah Anggota";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			$data['berkas'] = $this->anggota_model->getAllBerkas();

			$this->load->view('pages/tambah_anggota', $data);
			$this->load->view('_templates/footer', $data);
	}
	
	public function tambahAnggota()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);


		/*if($_FILE['bukti_pembayaran']):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
		endif;*/

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["tipe_user"=> "anggota","verifikasi"=>"belum","status_anggota"=>"nonaktif"]);
				$query = $this->db->insert("pengguna", $data);
				$id_anggota = $this->db->insert_id();
				$berkas = $this->anggota_model->getAllBerkas();
				$index = 1;
				foreach ($berkas as $row):
				
					if($_FILES['berkas-'.$row->id_berkas]):
						if ( ! $this->upload->do_upload('berkas-'.$row->id_berkas))
						{
								$err[] = $this->upload->display_errors();
						}
						else
						{
							$udata = array('upload_data' => $this->upload->data());
							$file_berkas[] = $udata['upload_data']['file_name'];
							$this->anggota_model->insertBerkas(["id_berkas" => $row->id_berkas, "id_anggota" => $id, "nama_file" => $udata['upload_data']['file_name']]);
						}
					endif;
					$index++;
				endforeach;
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
	}

	public function import_anggota()
	{
            $data['title'] = "Import Anggota";
            $data['page_title'] = "Import Anggota";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			$this->load->view('pages/import_anggota', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function importAnggota()
	{
		mb_internal_encoding("8bit"); 
		include 'application/libraries/excel_loader2.php';
		// upload file xls
		$target = 'uploads/'.$_FILES['filepegawai']['name'] ;
		move_uploaded_file($_FILES['filepegawai']['tmp_name'], $target);
		
		// beri permisi agar file xls dapat di baca
		//chmod($target,0777);
		
		// mengambil isi file xls
		$data = new Spreadsheet_Excel_Reader($target,false);
		//$data = new Spreadsheet_Excel_Reader('test_import.xls',false);
		//echo "<pre>"; print_r($data); echo "</pre>";
		// menghitung jumlah baris data yang ada
		$jumlah_baris = $data->rowcount($sheet_index=0);
		// jumlah default data yang berhasil di import
		$berhasil = 0;
		for ($i=2; $i<=$jumlah_baris; $i++){
		
			// menangkap data dan memasukkan ke variabel sesuai dengan kolumnya masing-masing
			$nama     = $data->val($i, 1);
			$alamat   = $data->val($i, 2);
			$telepon  = $data->val($i, 3);
		
			if($nama != "" && $alamat != "" && $telepon != ""){
				// input data ke database (table data_pegawai)
				$nama = ucwords(strtolower($nama));
				$username = str_replace(" ", "", strtolower($nama));
				$_data = ["nama" => $nama, "alamat"=>$alamat, "telepon"=>$telepon, "username" => $username, "password"=>$username,"tipe_user"=> "anggota","verifikasi"=>"belum","status_anggota"=>"nonaktif"];
				//print_r($data);
				$query = $this->db->insert("pengguna", $_data);
				$berhasil++;
			}
		}

		if($berhasil > 0)
		{
			$msg = "Data Berhasil Ditambah";
			$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		} else {
			$msg = "Gagal import data";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
		
		// hapus kembali file .xls yang di upload tadi
		//unlink($target);
	}

	
	public function ubah_anggota()
	{
            $data['title'] = "Ubah Anggota";
            $data['page_title'] = "Ubah Anggota";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['berkas'] = $this->anggota_model->getAllBerkas();
		
			$this->load->view('pages/ubah_anggota', $data);
			$this->load->view('_templates/footer', $data);
	}
	
	public function ubahAnggota()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);


		/*if($_FILE['bukti_pembayaran']):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
		endif;*/
		$id=$this->input->post('id_user');
		$data = $this->input->post();
		if(true) {
			unset($data['id_user']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["tipe_user"=> "anggota"]);
				$berkas = $this->anggota_model->getAllBerkas();
				$index = 1;
				foreach ($berkas as $row):
				
					if($_FILES['berkas-'.$row->id_berkas]):
						if ( ! $this->upload->do_upload('berkas-'.$row->id_berkas))
						{
								$err[] = $this->upload->display_errors();
						}
						else
						{
								$udata = array('upload_data' => $this->upload->data());
								$file_berkas[] = $udata['upload_data']['file_name'];
								$this->anggota_model->insertBerkas(["id_berkas" => $row->id_berkas, "id_anggota" => $id, "nama_file" => $udata['upload_data']['file_name']]);
						}
					endif;
					$index++;
				endforeach;
				$this->anggota_model->update($id,$data);
				$msg = "Data Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				if($this->session->userdata('type') == 'anggota'):
					redirect('home/data_diri', 'refresh');
				else:
					redirect('home/anggota', 'refresh');
				endif;
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/anggota', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/anggota', 'refresh');
		}
	}


	public function data_diri()
	{
            $data['title'] = "Data Diri";
            $data['page_title'] = "Data";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			$data['data'] = $this->anggota_model->getByID($this->session->userdata('id'))[0];
		
			$this->load->view('pages/data_diri', $data);
			$this->load->view('_templates/footer', $data);
	}
	


	public function sisa_hasil_usaha()
	{
            $data['title'] = "Sisa Hasil Usaha";
            $data['page_title'] = "Sisa Hasil Usaha";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['shu'] = $this->transaksi_model->getSisaHasilUsahaAll();
			$this->load->view('pages/sisa_hasil_usaha', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambah_shu()
	{
            $data['title'] = "Tambah Sisa Hasil Usaha";
            $data['page_title'] = "Tambah Sisa Hasil Usaha";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			//$data['shu'] = $this->transaksi_model->getSisaHasilUsahaAll();
			$this->load->view('pages/tambah_shu', $data);
			$this->load->view('_templates/footer', $data);
	} 

	public function tambahSHU()
    {

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				//$data = array_merge($data, ["status_anggota"=> "aktif"], ["verifikasi"=> "sudah"]);
				$this->db->insert("sisa_hasil_usaha",$data);
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/detail_shu?shu='.$this->db->insert_id(), 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/sisa_hasil_usaha', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/sisa_hasil_usaha', 'refresh');
		}
	}

	public function detail_shu()
	{
            $data['title'] = "Sisa Hasil Usaha";
            $data['page_title'] = "Sisa Hasil Usaha";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['shu'] = $this->transaksi_model->getSisaHasilUsahaByID($_GET['shu']);
			$this->load->view('pages/detail_shu', $data);
			$this->load->view('_templates/footer', $data);
	} 

	public function detail_sisa_hasil_usaha()
	{
            $data['title'] = "Detail Sisa Hasil Usaha";
            $data['page_title'] = "Detail Sisa Hasil Usaha";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['detail_shu'] = $this->transaksi_model->getDetailSisaHasilUsaha($_GET['shu']);
			$this->load->view('pages/detail_sisa_hasil_usaha', $data);
			$this->load->view('_templates/footer', $data);
	} 

		public function tambahDetailSHU()
		{
	
			$data = $this->input->post();
			if(true) {
				
				$pembagian = $data['persen_pembagian'];
				if(!isset($err))
				{
					$anggota = $this->anggota_model->getAll();
					$index = 0;
					foreach ($anggota as $row): 
					$persen = $pembagian[$index];
					$data = array_merge($data, ["id_anggota"=>$row->id_user, "persen_pembagian" => $persen]);
						if($this->transaksi_model->checkSHUByIDAndAnggota($data['id_sisa_hasil_usaha'], $row->id_user) > 0):

							$this->transaksi_model->updateDSHU($data, $row->id_user);
						else:

							$this->transaksi_model->insertDSHU($data);
						endif;
						
						//echo $index."= ";print_r($data); echo "<br>";
						$index++;
					endforeach;
					$msg = "Data Berhasil Ditambah";
					$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
					redirect('home/detail_sisa_hasil_usaha?shu='.$data['id_sisa_hasil_usaha'], 'refresh');
				}else {
					$msg = implode(" ", $err);
					$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
					redirect('home/detail_sisa_hasil_usaha?shu='.$data['id_sisa_hasil_usaha'], 'refresh');
				}
			}else {
				$msg = "Data tidak ada";
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/detail_sisa_hasil_usaha?shu='.$data['id_sisa_hasil_usaha'], 'refresh');
			}
		}

	
	public function biaya()
	{
            $data['title'] = "Biaya";
            $data['page_title'] = "Biaya";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['biaya'] = $this->transaksi_model->getBiayaAll();
			$this->load->view('pages/biaya', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function ubah_biaya()
	{
            $data['title'] = "Ubah Biaya";
            $data['page_title'] = "Ubah Biaya";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['edit'] = $this->transaksi_model->getBiayaByID($this->input->get('edit'));
			$this->load->view('pages/ubah_biaya', $data);
			$this->load->view('_templates/footer', $data);
	}
	

	public function ubahBiaya()
    {

		$id=$this->input->post('kode_biaya');

		$data = $this->input->post();
		if(true) {
			unset($data['kode_biaya']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				//$data = array_merge($data, ["tipe_user"=> "anggota"]);
				$this->transaksi_model->updateBiaya($id,$data);
				$msg = "Data Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/biaya', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/biaya', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/biaya', 'refresh');
		}
	}

	public function metadata()
	{
            $data['title'] = "Metadata";
            $data['page_title'] = "Metadata";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['metadata'] = $this->transaksi_model->getMetadataAll();
			$this->load->view('pages/metadata', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function ubah_metadata()
	{
            $data['title'] = "Ubah Metadata";
            $data['page_title'] = "Ubah Metadata";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['edit'] = $this->transaksi_model->getMetadataByID($this->input->get('edit'));
			$this->load->view('pages/ubah_metadata', $data);
			$this->load->view('_templates/footer', $data);
	}
	

	public function ubahMetadata()
    {

		$id=$this->input->post('nama_metadata');

		$data = $this->input->post();
		if(true) {
			unset($data['nama_metadata']);
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				//$data = array_merge($data, ["tipe_user"=> "anggota"]);
				$this->transaksi_model->updateMetadata($id,$data);
				$msg = "Data Berhasil Diubah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/metadata', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/metadata', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/metadata', 'refresh');
		}
	}
    
    public function transaksi()
	{
            $data['title'] = "Transaksi";
            $data['page_title'] = "Transaksi";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			

			if($this->session->userdata("type") == "anggota"):
				$data['transaksi'] = $this->transaksi_model->getAllByAnggota($this->session->userdata("id"));
			else:
				$data['transaksi'] = $this->transaksi_model->getAll();
			endif;

			$this->load->view('pages/transaksi', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function simpanan()
	{
            $data['title'] = "Simpanan";
            $data['page_title'] = "Simpanan";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);

			if($this->session->userdata("type") == "anggota"):
				$data['simpanan'] = $this->transaksi_model->getSimpananByAnggota($this->session->userdata("id"));
			else:
				$data['simpanan'] = $this->transaksi_model->getSimpananAll();
			endif;

			$this->load->view('pages/simpanan', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambah_simpanan()
	{
            $data['title'] = "Simpanan";
            $data['page_title'] = "Simpanan";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$this->load->view('pages/tambah_simpanan', $data);
			$this->load->view('_templates/footer', $data);
	}

	
	public function konfirmasiSimpanan()
    {
		$data = ["status_simpanan" => "terverifikasi"];
		$this->transaksi_model->updateSimpanan( $this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Berhasil Melakukan Simpanan");
		$msg = "Simpanan Berhasil Dikonfirmasi";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/simpanan', 'refresh');

	}

	public function tolakSimpanan()
    {
		$data = ["status_simpanan" => "ditolak"];
		$this->transaksi_model->updateSimpanan( $this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Simpanan ditolak, mohon pastikan uang telah ditransfer");
		$msg = "Simpanan Ditolak";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/simpanan', 'refresh');

	}

	public function tambahSimpanan()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$bukti_pembayaran = "";

		//if($_FILE['bukti_pembayaran']):
			if ( ! $this->upload->do_upload('bukti_pembayaran'))
			{
					$err[] = $this->upload->display_errors();
			}
			else
			{
					$data = array('upload_data' => $this->upload->data());
					$bukti_pembayaran = $data['upload_data']['file_name'];
			}
		//endif;

		$data = $this->input->post();
		if(true) {

			$jumlah_simpanan = $data['jumlah_simpanan'];
			//if($this->transaksi_model->getMetadata('batas_simpanan_wajib') <= date("d")){
				//$total_simpanan = $jumlah_simpanan + ($jumlah_simpanan * $this->transaksi_model->getBiaya('bunga_bulanan'));
			//}
			unset($data['jumlah_simpanan']);

			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["id_anggota" => $this->session->userdata("id"), "jumlah_simpanan" => $jumlah_simpanan,  "bukti_pembayaran" => $bukti_pembayaran]);

				$this->transaksi_model->insertSimpanan($data);
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/simpanan', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/simpanan', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/simpanan', 'refresh');
		}
	}

	public function pinjaman()
	{
            $data['title'] = "Pinjaman";
            $data['page_title'] = "Pinjaman";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			
			if($this->session->userdata("type") == "anggota"):
				$data['pinjaman'] = $this->transaksi_model->getPinjamanByAnggota($this->session->userdata("id"));
			else:
				$data['pinjaman'] = $this->transaksi_model->getPinjamanAll();
			endif;
			$this->load->view('pages/pinjaman', $data);
			$this->load->view('_templates/footer', $data);
	}
	

	public function konfirmasiPinjaman()
    {
		$data = ["status_pinjaman" => "terverifikasi"];
		$this->transaksi_model->updatePinjaman( $this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Berhasil Melakukan Pinjaman");
		$msg = "Pinjaman Berhasil Dikonfirmasi";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/pinjaman?', 'refresh');

	}

	public function konfirmasiPinjamanDikirim()
    {
		$data = ["status_pinjaman" => "diterima"];
		$this->transaksi_model->updatePinjaman( $this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Uang Pinjaman Berhasil Ditransfer");
		$msg = "Pengiriman Pinjaman Berhasil Dikonfirmasi";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/pinjaman?', 'refresh');

	}

	public function tolakPinjaman()
    {
		$data = ["status_pinjaman" => "ditolak"];
		$this->transaksi_model->updatePinjaman($this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Pinjaman ditolak, mohon periksa kembali nominal pinjaman anda ");
		$msg = "Pinjaman Ditolak";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/pinjaman', 'refresh');

	}

	public function tambah_pinjaman()
	{
            $data['title'] = "Pinjaman";
            $data['page_title'] = "Pinjaman";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$this->load->view('pages/tambah_pinjaman', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function detail_pinjaman()
	{
            $data['title'] = "Data Pinjaman";
            $data['page_title'] = "Data Pinjaman";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$data['pinjaman'] = $this->transaksi_model->getPinjamanByID($this->input->get("id"));
			$data['pengembalian'] = $this->transaksi_model->getPengembalianPinjamanByPinjaman($this->input->get("id"));
			$this->load->view('pages/detail_pinjaman', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambahPinjaman()
    {

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["id_anggota" => $this->session->userdata("id")]);
				$this->transaksi_model->insertPinjaman($data);
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pinjaman', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pinjaman', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pinjaman', 'refresh');
		}
	}

	public function tambah_cicilan()
	{
            $data['title'] = "Cicilan";
            $data['page_title'] = "Cicilan";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$this->load->view('pages/tambah_cicilan', $data);
			$this->load->view('_templates/footer', $data);
	}

	public function tambahCicilan()
    {
		$config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|jpeg|png|doc|docx|pdf';
		$config['encrypt_name']         = true;
		$config['max_size']             = '10000'; //kilobyte
		$this->load->library('upload', $config);

		$bukti_pembayaran = "";

		if ( ! $this->upload->do_upload('bukti_pembayaran'))
		{
				$err[] = $this->upload->display_errors();
		}
		else
		{
				$data = array('upload_data' => $this->upload->data());
				$bukti_pembayaran = $data['upload_data']['file_name'];
		}

		$data = $this->input->post();
		if(true) {
			foreach($data as $item=>$value){
				if($value == ""){
					$err[] = ucfirst($item) . " tidak boleh kosong!";
				}
			}

			if(!isset($err))
			{
				$data = array_merge($data, ["bukti_pembayaran" => $bukti_pembayaran, "tanggal" => date("Y-m-d"), "status_pengembalian" => "menunggu"]);
				$this->transaksi_model->insertCicilan($data);
				$msg = "Data Berhasil Ditambah";
				$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
				redirect('home/pinjaman', 'refresh');
			}else {
				$msg = implode(" ", $err);
				$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
				redirect('home/pinjaman', 'refresh');
			}
		}else {
			$msg = "Data tidak ada";
			$this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
			redirect('home/pinjaman', 'refresh');
		}
	}

	public function konfirmasiCicilan()
    {
		$data = ["status_pengembalian" => "diterima"];
		$this->transaksi_model->updateCicilan( $this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Cicilan Berhasil Diverifikasi");
		$msg = "Pengiriman Cicilan Berhasil Dikonfirmasi";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/pinjaman?', 'refresh');

	}

	public function tolakCicilan()
    {
		$data = ["status_pengembalian" => "ditolak"];
		$this->transaksi_model->updateCicilan($this->input->get("id"), $data);
		$this->anggota_model->insertNotif($this->input->get("id_user"), "Cicilan ditolak, mohon periksa kembali nominal pengembalian anda ");
		$msg = "Pinjaman Ditolak";
		$this->session->set_flashdata('msg', array('type' => 'success', 'message' => $msg));
		redirect('home/pinjaman', 'refresh');

	}

	public function rekening()
	{
            $data['title'] = "Rekening Koran";
            $data['page_title'] = "Rekening Koran";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			

			$this->load->view('pages/rekening', $data);
			$this->load->view('_templates/footer', $data);
	}


	public function cetakRekeningKoran()
	{
			$data['transaksi'] = $this->transaksi_model->getAllByAnggotaBulanTahun($this->input->get('id'),$this->input->get('bulan'),$this->input->get('tahun'));
			$data['anggota'] = $this->anggota_model->getByID($this->input->get('id'));

			$html = $this->load->view('laporanRekeningKoran',$data, true);
	
			$mpdf = new \Mpdf\Mpdf([
				'mode' => 'utf-8',
				'format' => 'A4', //F4
				'orientation' => 'L',
				'margin_left' => 2,
				'margin_right' => 2,
				'margin_top' => 5,
				'margin_bottom' => 5,
				'tempDir' => './uploads'
				]);
			$mpdf->AddPage('P');
			$mpdf->WriteHTML($html);
			if($save)
			{
				$mpdf->Output('./report.pdf','F');
			}else {
				$mpdf->Output();
			}
		}
}
