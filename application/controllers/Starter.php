<?php
defined('BASEPATH') OR exit('Access Denied!');

class Starter extends CI_Controller {

	public function index()
	{
			$data['title'] = "Starter";
			$this->load->view('_templates/header', $data);
			$this->load->view('_templates/navbar', $data);
			$this->load->view('pages/starter', $data);
			$this->load->view('_templates/footer', $data);
	}
}
