<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
<style>
    body {
        font-family: times new roman;
    }
    .header {
        border-bottom: 4px groove black;
    }
	body {
		margin: 0;
        font-size: 10pt;
	}

    .border {
        border: groove 2px black;
        padding: 5px;
    }

	h1 {
		font-size: 10pt;
		text-align:center;
        margin:0;
        padding:0;
	}
	
    table {
		text-align:center;
		width:100%
    }

	td {
		padding: 5px;
		text-align: center;
	}
    img {
        width:100px;
    }
</style>
</head>
<body>

<?php
function dateCodeToText($date){
    $date = str_replace('Jan', 'Januari', $date);
    $date = str_replace('Feb', 'Februari', $date);
    $date = str_replace('Mar', 'Maret', $date);
    $date = str_replace('Apr', 'April', $date);
    $date = str_replace('May', 'Mei', $date);
    $date = str_replace('Jun', 'Juni', $date);
    $date = str_replace('Jul', 'Juli', $date);
    $date = str_replace('Aug', 'Agustus', $date);
    $date = str_replace('Sep', 'September', $date);
    $date = str_replace('Oct', 'Oktober', $date);
    $date = str_replace('Nov', 'November', $date);
    $date = str_replace('Dec', 'Desember', $date);
    return $date;
}
?>
<b>Nama Anggota: <?=$anggota[0]->nama;?></b>
<h1>Rekening Koran <?=dateCodeToText(date("M Y", strtotime(date($_GET['tahun'] . "-" . $_GET['bulan']))));?></h1>
<br>
</br>
<div>
                                           <table border="1" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tanggal</th>
                                                    <th>Tipe Transaksi</th>
                                                    <th>Debet</th>
                                                    <th>Kredit</th>
                                                </tr>
                                            </thead>    
                                            <tbody>
                                            <?php $no = 1; $debet = 0; $kredit = 0; if($transaksi != ""): foreach($transaksi as $row):?>
                                                <tr>
                                                <td><?=$no;?></td>
                                                <td><?=$row->tanggal;?></td>
                                                <td><?=$row->tipe;?>
                                                <td><?=number_format($row->debet);?></td>
                                                <td><?=number_format($row->kredit);?></td>
                                                </tr>
                                                <?php $debet += $row->debet; $kredit += $row->kredit; $no++; endforeach; endif;?>

                                            </tbody>

                                            <tfoot>
                                                <tr>
                                                <td colspan="3" align="right">Total</td>
                                                <td><?=number_format($debet);?></td>
                                                <td><?=number_format($kredit);?></td>
                                                </tr>

                                                <tr>
                                                <td colspan="3" align="right">Sisa Saldo</td>
                                                <td colspan="2"><?=number_format($debet-$kredit);?></td>
                                                </tr>
                                            </tfoot>
                                        </table>
</div>
</body>
</html> 