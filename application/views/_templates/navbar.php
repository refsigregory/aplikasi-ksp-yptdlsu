
<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="<?=base_url('home');?>"><i class="fa fa-dashboard fa-fw"></i> Beranda</a>
            </li>
        <?php if($this->anggota_model->getByID($this->session->userdata("id"))[0]->status_anggota == "aktif"):?>
            <?php if($this->session->userdata("type") == "admin"):?>
            <li>
                <a href="<?=base_url('home/pengguna');?>"><i class="fa fa-users fa-fw"></i> Pengguna</a>
            </li>
            <?php endif;?>

            <?php if($this->session->userdata("type") == "admin" || $this->session->userdata("type") == "pengurus"):?>
            <li>
                <a href="<?=base_url('home/anggota');?>"><i class="fa fa-users fa-fw"></i> Anggota</a>
            </li>
            <?php endif;?>

            <?php if($this->session->userdata("type") == "admin"):?>
            <li>
                <a href="<?=base_url('home/biaya');?>"><i class="fa fa-gear fa-fw"></i> Biaya</a>
            </li>
            <?php endif;?>

            <?php if($this->session->userdata("type") == "admin"):?>
            <li>
                <a href="<?=base_url('home/metadata');?>"><i class="fa fa-gear fa-fw"></i> Metadata</a>
            </li>
            <?php endif;?>

            <?php if($this->session->userdata("type") == "admin"):?>
            <li>
                <a href="<?=base_url('home/sisa_hasil_usaha');?>"><i class="fa fa-money fa-fw"></i> Sisa Hasil Usaha (SHU)</a>
            </li>
            <?php endif;?>

            <?php if($this->session->userdata("type") == "anggota"):?>
            <li>
                <a href="<?=base_url('home/data_diri');?>"><i class="fa fa-user fa-fw"></i> Data Diri</a>
            </li>
            <?php endif;?>
            

            <li>
                <a href="<?=base_url('home/simpanan');?>"><i class="fa fa-money fa-fw"></i> Simpanan</a>
            </li>

            <li>
                <a href="<?=base_url('home/pinjaman');?>"><i class="fa fa-money fa-fw"></i> Pinjaman</a>
            </li>

            
            <?php if($this->session->userdata("type") == "admin"  || $this->session->userdata("type") == "pengurus"):?>
            <li>
                <a href="<?=base_url('home/rekening');?>"><i class="fa fa-book fa-fw"></i> Rekening Koran</a>
            </li>
            <?php endif;?>



            <li>
                <a href="<?=base_url('home/transaksi');?>"><i class="fa fa-table fa-fw"></i> Riwayat Transaksi</a>
            </li>
            <?php else:?>
            <li>
                <a href="<?=base_url('auth/aktivasi');?>"><i class="fa fa-table fa-fw"></i> Formulir Data Diri</a>
            </li>
            <?php  endif;?>
            
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
<!-- /.navbar-static-side -->
</nav>
