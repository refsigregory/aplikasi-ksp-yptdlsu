
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Anggota 
                                    <?php if($this->session->userdata("type") == "pengurus"):?>
                                    <a href="<?=base_url('home/tambah_anggota');?>" class="btn btn-primary">Tambah</a>
                                    <a href="<?=base_url('home/import_anggota');?>" class="btn btn-primary">Import</a>
                                    <?php endif;?>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Anggota</th>
                                                    <th>Jenis Kelamin</th>
                                                    <th>Alamat</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($anggota != ""): foreach($anggota as $row):?>
                                                <td><?=$no;?></td>
                                                <td><?=$row->nama;?></td>
                                                <td><?=$row->jenis_kelamin;?></td>
                                                <td><?=$row->alamat;?></td>
                                                <td><?=$row->status_anggota;?></td>
                                                <td>


                                                        <a href="<?=base_url('home/anggota?view='.$row->id_user);?>">
                                                        <button class="btn btn-sm btn-info" title="Lihat">Lihat</button>
                                                        </a>

                                                        
                                                        <?php if($this->session->userdata("type") == "pengurus"):?>
                                                        
                                                        <?php if($row->status_anggota == 'menunggu'): ?>
                                                        <a href="<?=base_url('home/anggota?aktif='.$row->id_user);?>">
                                                            <button class="btn btn-sm btn-success" title="Lihat">Konfirmasi</button>
                                                        </a>
                                                        <?php endif; ?>

                                                        <a href="<?=base_url('home/ubah_anggota?edit='.$row->id_user);?>">
                                                        <button class="btn btn-sm btn-warning" title="Ubah">Ubah</button>
                                                        </a>
                                                        <?php endif;?>
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php
if(isset($_GET['view']))
{
    $this->load->view('modal_detail_anggota');
}


?>