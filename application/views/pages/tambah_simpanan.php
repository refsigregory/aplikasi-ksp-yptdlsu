
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>

                            <?=form_open_multipart(base_url('home/tambahSimpanan'));?>
                <div class="form-group">
                    <label>Tipe Simpanan</label>
                    <select class="form-control" name="jenis_simpanan" >
                        <option>Simpanan Wajib</option>
                        <option>Simpanan Pokok</option>
                        <option>Simpanan Sukarela</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Jumlah Simpanan</label>
                    <input type="text" class="form-control" name="jumlah_simpanan" placeholder="100000">
                </div>

                <div class="form-group">
                    <label>Tanggal</label>
                    <input type="text" class="form-control" name="tanggal" value="<?=date("Y-m-d");?>">
                </div>

                <div class="form-group">
                    <label>Bukti Pembayaran</label>
                    <input type="file" class="form-control" name="bukti_pembayaran" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Simpan</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->