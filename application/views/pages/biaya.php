
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Biaya
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Biaya</th>
                                                    <th>Jumlah Biaya</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($biaya != ""): foreach($biaya as $row):?>
                                            <tr>
                                                <td><?=$no;?></td>
                                                <td><?=$row->nama_biaya;?>
                                                
                                                <td><?=($row->tipe_biaya == "persen") ? $row->jumlah_biaya . '%' : number_format($row->jumlah_biaya);?></td>
                                                <td>
                                                    <a href="<?=base_url('home/ubah_biaya?edit='.$row->kode_biaya);?>">
                                                        <button class="btn btn-sm btn-warning" title="Ubah">Ubah</button>
                                                        </a>
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>
                                                    
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->