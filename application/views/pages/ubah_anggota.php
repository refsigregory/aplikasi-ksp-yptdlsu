<?php
    $edit = $this->anggota_model->getByID($this->input->get('edit'))[0];
  ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>
                            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                            <?=form_open_multipart(base_url('home/ubahAnggota'));?>

                            <input type="hidden" name="id_user" value="<?=$edit->id_user;?>" placeholder="">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" value="<?=$edit->username;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="form-control" name="password" value="<?=$edit->password;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Email</label>
                    <input type="text" class="form-control" name="email" value="<?=$edit->email;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Telepon</label>
                    <input type="text" class="form-control" name="telepon" value="<?=$edit->telepon;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" value="<?=$edit->nama;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Jenis Kelamin</label>
                    <select class="form-control" name="jenis_kelamin" id="">
                    <option value=""> - Pilih Jenis Kelamin - </option>
                    <option value="Laki-laki" <?=($edit->jenis_kelamin == 'Laki-laki') ? 'selected="selected"' : '';?>>Laki-laki</option>
                    <option value="Perempuan" <?=($edit->jenis_kelamin == 'Perempuan') ? 'selected="selected"' : '';?>>Perempuan</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <input type="text" class="form-control" name="tanggal_lahir" value="<?=$edit->tanggal_lahir;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Status Alamat</label>
                    <select class="form-control" name="status_alamat" id="">
                    <option <?=($edit->status_alamat == 'Rumah Sendiri') ? 'selected="selected"' : '';?>>Rumah Sendiri</option>
                    <option <?=($edit->status_alamat == 'Rumah Kontrak') ? 'selected="selected"' : '';?>>Rumah Kontrak</option>
                    <option <?=($edit->status_alamat == 'Kost') ? 'selected="selected"' : '';?>>Kost</option>
                    <option <?=($edit->status_alamat == 'Rumah Saudara') ? 'selected="selected"' : '';?>>Rumah Saudara</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Alamat</label>
                    <input type="text" class="form-control" name="alamat" value="<?=$edit->alamat;?>"placeholder="">
                </div>
                
                <?php if($this->session->userdata('type') == "pengurus"):?>
                <div class="form-group">
                    <label>Status Anggota</label>
                    <select class="form-control" name="status_anggota" id="">
                    <option <?=($edit->status_anggota == 'aktif') ? 'selected="selected"' : '';?>>aktif</option>
                    <option <?=($edit->status_anggota == 'nonaktif') ? 'selected="selected"' : '';?>>nonaktif</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Status Verifikasi</label>
                    <select class="form-control" name="verifikasi" id="">
                    <option <?=($edit->verifikasi == 'sudah') ? 'selected="selected"' : '';?>>sudah</option>
                    <option <?=($edit->verifikasi == 'belum') ? 'selected="selected"' : '';?>>belum</option>
                    </select>
                </div>

                <?php endif;?>

                <?php foreach($berkas as $row):?>
                            <div class="form-group">
                                <label><?=$row->nama_berkas;?></label>
                                <input class="form-control" type="file" placeholder="<?=$row->nama_berkas;?>" name="berkas-<?=$row->id_berkas;?>">
                            </div>
                            <?php endforeach;?>


                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->