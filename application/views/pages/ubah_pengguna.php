<?php
    $edit = $this->anggota_model->getByID($this->input->get('edit'))[0];
  ?>
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>
                            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                            <?=form_open_multipart(base_url('home/ubahPengguna'));?>

                            <input type="hidden" name="id_user" value="<?=$edit->id_user;?>" placeholder="">
                <div class="form-group">
                    <label>Username</label>
                    <input type="text" class="form-control" name="username" value="<?=$edit->username;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Password</label>
                    <input type="text" class="form-control" name="password" value="<?=$edit->password;?>"placeholder="">
                </div>

                <div class="form-group">
                    <label>Nama Lengkap</label>
                    <input type="text" class="form-control" name="nama" value="<?=$edit->nama;?>"placeholder="">
                </div>


                <div class="form-group">
                    <label>Status Anggota</label>
                    <select class="form-control" name="status_anggota" id="">
                    <option <?=($edit->status_anggota == 'aktif') ? 'selected="selected"' : '';?>>aktif</option>
                    <option <?=($edit->status_anggota == 'nonaktif') ? 'selected="selected"' : '';?>>nonaktif</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Tipe User</label>
                    <select class="form-control" name="tipe_user" id="">
                    <option <?=($edit->tipe_user == 'admin') ? 'selected="selected"' : '';?>>admin</option>
                    <option <?=($edit->tipe_user == 'pengurus') ? 'selected="selected"' : '';?>>pengurus</option>
                    </select>
                </div>


                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->