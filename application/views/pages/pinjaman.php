
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Pinjaman
                                    <?php if($this->session->userdata("type") == "anggota"):?>
                                    <a href="<?=base_url('home/tambah_pinjaman');?>" class="btn btn-primary">Ajukan Pinjaman</a>
                                    <?php endif;?>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <?php if($this->session->userdata("type") == "admin" || $this->session->userdata("type") == "pengurus"):?>
                                                    <th>Nama Anggota</th>
                                                    <?php endif;?> 
                                                    <th>Jumlah Pinjaman</th>
                                                    <th>Tanggal Pinjaman</th>
                                                    <th>Tanggal Pengembalian</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($pinjaman != ""): foreach($pinjaman as $row):?>
                                                <td><?=$no;?></td>
                                                <?php if($this->session->userdata("type") == "pengurus"):?>
                                                   <td><?=$this->anggota_model->getByID($row->id_anggota)[0]->nama;?></td>
                                                <?php endif;?> 
                                                <td><?=number_format($row->jumlah_pinjaman);?></td>
                                                <td><?=$row->tanggal_pinjaman;?></td>
                                                <td><?=$row->tanggal_pengembalian;?></td>
                                                <td><?=$row->status_pinjaman;?></td>
                                                <td>
                                                    <a href="<?=base_url('home/detail_pinjaman?id='.$row->id_pinjaman.'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-info" title="Lihat Detail"><i class="fa fa-window-pencil"></i>Lihat</button>
                                                    </a>

                                                    <?php if($this->session->userdata("type") == "pengurus"):?>
                                                        <?php if($row->status_pinjaman == "menunggu"):?>
                                                    <a href="<?=base_url('home/konfirmasiPinjaman?id='.$row->id_pinjaman.'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-success" title="Konfirmasi Pinjaman">Konfirmasi</button>
                                                    </a>
                                                    
                                                    <a href="<?=base_url('home/tolakPinjaman?id='.$row->id_pinjaman.'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-danger" title="Tolak Permintaan"><i class="fa fa-window-pencil"></i>Tolak</button>
                                                    </a>
                                                        <?php elseif($row->status_pinjaman == "terverifikasi"):?>
                                                    <a href="<?=base_url('home/konfirmasiPinjamanDikirim?id='.$row->id_pinjaman.'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-success" title="Konfirmasi Pinjaman">Konfirmasi Pengiriman</button>
                                                    </a> 
                                                        <?php endif;?>
                                                    <?php endif;?>
                            
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->