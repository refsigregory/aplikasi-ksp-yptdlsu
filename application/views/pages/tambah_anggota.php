
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>

                            <?=form_open_multipart(base_url('home/tambahAnggota'));?>
                            <div class="form-group">
                                <input class="form-control" placeholder="Nama Pengguna" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Kata Sandi" name="password" type="password" value="">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Email" name="email" type="email">
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Telepon" name="telepon" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Nama Lengkap (Sesuai KTP/SIM/KK)" name="nama" type="text">
                            </div>
                            
                            <div class="form-group">
                                <input class="form-control" placeholder="Nama Panggilan" name="nama_panggilan" type="text" autofocus>
                            </div>

                            
                            <div class="form-group">
                            <select class="form-control" name="jenis_kelamin" id="">
                                 <option value=""> - Pilih Jenis Kelamin - </option>
                                 <option value="Laki-laki">Laki-laki</option>
                                 <option value="Perempuan">Perempuan</option>
                                 </select>
                            </div>

                            <div class="form-group">
                            <select class="form-control" name="tipe_anggota" id="">
                                 <option value=""> - Pilih Jenis Anggoa - </option>
                                 <option value="Karyawan">Karyawan</option>
                                 <option value="Mahasiswa">Mahasiswa</option>
                                 </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Tempat Lahir" name="tempat_lahir" type="text">
                            </div>
                            
                            <div class="form-group">
                                <input class="form-control" placeholder="Tanggal Lahir (YYYY-mm-dd)" name="tanggal_lahir" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Nomor KTP/SIM/KK yang masih berlaku" name="nomor_identitas" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Alamat Sekarang" name="alamat" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Tahun Menetap" name="tahun_menetap" type="text" value="">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Lingkungan" name="lingkungan" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Kelurahan" name="kelurahan" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Kecamatan" name="kecamatan" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Kota" name="kota" type="text">
                            </div>
                            
                            <div class="form-group">
                                <select class="form-control" name="status_alamat" id="">
                                <option value=""> - Status Alamat -</option>
                                <option>Rumah Sendiri</option>
                                <option>Rumah Kontrak</option>
                                <option>Kost</option>
                                <option>Rumah Saudara</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Pekerjaan" name="pekerjaan" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Tahun Masuk Kerja" name="tahun_masuk_kerja" type="text">
                            </div>

                            <div class="form-group">
                                <input class="form-control" placeholder="Alamat Tempat Kerja Sekarang" name="alamat_tempat_kerja" type="text">
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="status_alamat" id="">
                                <option value=""> - Penghasilan Per Bulan -</option>
                                <option>s/d 1 juta</option>
                                <option>1 s/d 2 juta</option>
                                <option>2 s/d 3,5 juta</option>
                                <option>3,5 s/d 5 juta</option>
                                <option>5 s/d 10 juta</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="status_pernikahan" id="">
                                <option value=""> - Status Pernikahan -</option>
                                <option>Belum Menikah</option>
                                <option>Menikah</option>
                                <option>Janda</option>
                                <option>Duda</option>
                                <option>Biarawan/ti</option>
                                </select>
                            </div>

                <div class="form-group">
                    <label>Status Anggota</label>
                    <select class="form-control" name="status_anggota" id="">
                    <option>nonaktif</option>
                    <option>aktif</option>
                    </select>
                </div>

                <!--<div class="form-group">
                    <label>Status Verifikasi</label>
                    <select class="form-control" name="verifikasi" id="">
                    <option>sudah</option>
                    <option>belum</option>
                    </select>
                </div>-->

                <?php foreach($berkas as $row):?>
                            <div class="form-group">
                                <label><?=$row->nama_berkas;?></label>
                                <input class="form-control" type="file" placeholder="<?=$row->nama_berkas;?>" name="berkas-<?=$row->id_berkas;?>">
                            </div>
                            <?php endforeach;?>

                <input type="hidden" name="verifikasi" value="sudah" />


                <button type="submit" class="btn btn-primary btn-block">Tambah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->