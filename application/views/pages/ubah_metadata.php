            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>
                            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                            <?=form_open_multipart(base_url('home/ubahMetadata'));?>

                <input type="hidden" name="nama_metadata" value="<?=$edit[0]->nama_metadata;?>" placeholder="">
                
                <div class="form-group">
                    <label>Nama Metadata</label>
                    <input type="text" class="form-control" name="nama_metadata" value="<?=$edit[0]->nama_metadata;?>" readonly="">
                </div>

                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" class="form-control" name="keterangan" value="<?=$edit[0]->keterangan;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Jumlah Metadata</label>
                    <input type="text" class="form-control" name="metadata" value="<?=$edit[0]->metadata;?>" placeholder="">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->