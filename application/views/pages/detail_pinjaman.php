
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
           
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Data Pinjaman
                                    <?php if($this->session->userdata("type") == "anggota"):?>
                                    <a href="<?=base_url('home/tambah_cicilan?id='.$pinjaman[0]->id_pinjaman);?>" class="btn btn-primary">Bayar Cicilan</a>
                                    <?php endif;?>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>                    <div class="">

                                        <label>Nomor Rekening</label>:
                                        <?=$pinjaman[0]->nomor_rekening;?>
                                        <br>
                                        <label>Nama Rekening</label>:
                                        <?=$pinjaman[0]->nama_rekening;?>
                                        <br>
                                        <label>Jumlah Pinjaman</label>:
                                        <?=number_format($pinjaman[0]->jumlah_pinjaman);?>
                                        <br>
                                    </div>

                <?php $cek =$this->transaksi_model->checkPinjamanTerlambatByID($pinjaman[0]->id_pinjaman); if($cek != ""):
                        $denda = ($cek->jumlah_pinjaman * $this->transaksi_model->getBiaya('denda_pinjaman'));
                    ?>
                    <div class="alert alert-danger">Anda telah terlambat membayar pinjamanan, dengan ini anda dikenanan denda sebesar <?=number_format($denda);?></div>
                <?php endif;?>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No Transaksi</th> 
                                                    <th>Tanggal</th>
                                                    <th>Nominal</th>
                                                    <th>Bukti Pembayaran</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($pengembalian != ""): foreach($pengembalian as $row):?>
                                                <td><?=$row->id_pengembalian_pinjaman;?></td>
                                                
                                                <td><?=$row->tanggal;?></td>
                                                <td><?=number_format($row->nominal);?></td>
                                                <td>
                                                    <?php if($row->bukti_pembayaran != ""):?>
                                                        <a href="<?=base_url('uploads/'.$row->bukti_pembayaran);?>" target="_blank"><span class="label label-success">Lihat</span></a>
                                                    <?php else:?>
                                                        <span class="label label-danger">Tidak ada</span>
                                                    <?php endif;?>
                                                </td>
                                                <td><?=$row->status_pengembalian;?></td>
                                                <td>
                                                    <?php if($this->session->userdata("type") == "pengurus"):?>
                                                    <a href="<?=base_url('home/konfirmasiCicilan?id='.$row->id_pengembalian_pinjaman .'&id_user=');?>">
                                                    <button class="btn btn-sm btn-success" title="Konfirmasi Pengembalian">Konfirmasi</button>
                                                    </a>
                                                    
                                                    
                                                    <a href="<?=base_url('home/tolakCicilan?id='.$row->id_pengembalian_pinjaman .'&id_user=');?>">
                                                    <button class="btn btn-sm btn-danger" title="Tolak"><i class="fa fa-window-pencil"></i>Tolak</button>
                                                    </a>
                                                    <?php endif;?>    
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->