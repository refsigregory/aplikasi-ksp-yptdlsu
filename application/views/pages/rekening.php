
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
           
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Cetak Rekening Koran
                                    
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>   
<form action="<?=base_url('home/cetakRekeningKoran');?>" target="_blank">
<div class="form-group">
		<label>Anggota</label>:
		<?php
			$anggota = $this->anggota_model->getAll();
		?>
		<select name="id" class="form-control">
			<?php foreach($anggota as $row):?>
				<option value="<?=$row->id_user?>"><?=$row->nama?></option>
            <?php endforeach;?>
		</select>
	</div>
	<div class="form-group">
		<label>Bulan</label>:
		<?php
			$bulan = [
				"Januari",
				"Februari",
				"Maret",
				"April",
				"Mei",
				"Juni",
				"Juli",
				"Agustus",
				"September",
				"Oktober",
        "November",
        "Desember"
			];
		?>
		<select name="bulan" class="form-control">
			<?php for($i=0; $i< count($bulan); $i++):?>
				<option value="<?=$i+1;?>"><?=date("M", strtotime(date("Y") . "-" . ($i+1) . "-" . date("d")));?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<label>Tahun</label>:
		<select name="tahun" class="form-control">
			<?php for($tahun = date("Y"); $tahun>2000; $tahun--):?>
				<option><?=$tahun;?></option>
			<?php endfor;?>
		</select>
	</div>
	<div class="form-group">
		<button class="btn btn-primary">Cetak</button>
	</div>
</form>


</div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->