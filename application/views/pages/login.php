
<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title" style="text-align:center"><img src="<?=base_url();?>assets/logo_yys.png" width="80%"><br><?=$this->config->item("site_name");?></h3>
                </div>
                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>

                <div class="panel-body">
                    <form role="form" method="post" action="<?=base_url('auth/checkLogin');?>">
                        <fieldset>
                            <div class="form-group">
                                <input class="form-control" placeholder="Nama Pengguna" name="username" type="text" autofocus>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Kata Sandi" name="password" type="password" value="">
                            </div>
                            <!-- Change this to a button or input when using this as a form -->
                            <button class="btn btn-lg btn-success btn-block" type="submit" name="submit">Masuk</button>
                            <br>
                            <!--<a href="<?=base_url('auth/register');?>" class="btn btn-lg btn-info btn-block">Buat Akun</a>-->
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>