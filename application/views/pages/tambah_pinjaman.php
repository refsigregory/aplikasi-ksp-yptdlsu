
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>

                            <?=form_open_multipart(base_url('home/tambahPinjaman'));?>
                <div class="form-group">
                    <label>Nama Rekening</label>
                    <input type="text" class="form-control" name="nama_rekening" placeholder="">
                </div>

                <div class="form-group">
                    <label>Nama Bank</label>
                    <input type="text" class="form-control" name="nama_bank" placeholder="">
                </div>

                <div class="form-group">
                    <label>Nomor Rekening</label>
                    <input type="text" class="form-control" name="nomor_rekening" placeholder="">
                </div>

                <div class="form-group">
                    <label>Memohon Pinjaman Sebesar</label>
                    <input type="text" class="form-control" name="jumlah_pinjaman" placeholder="100000">
                </div>

                <div class="form-group">
                    <label>Tujuan Pinjaman</label>
                    <input type="text" class="form-control" name="tujuan_pinjaman">
                </div>

                <div class="form-group">
                    <input type="hidden" class="form-control" name="tanggal_pinjaman" value="<?=date("Y-m-d");?>">
                </div>

                <div class="form-group">
                    <label>Waktu Pengembalian</label>
                    <input type="text" class="form-control" name="tanggal_pengembalian" value="<?=date("Y-m-d");?>">
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ajukan Pinjaman</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->