
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Transaksi
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Anggota</th>
                                                    <th>Tipe Transaksi</th>
                                                    <th>Debet</th>
                                                    <th>Kredit</th>
                                                    <th>Tanggal</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($transaksi != ""): foreach($transaksi as $row):?>
                                                <td><?=$no;?></td>
                                                <td><?=$this->anggota_model->getByID($row->id_anggota)[0]->nama;?></td>
                                                <td><?=$row->tipe;?>
                                                <td><?=number_format($row->debet);?></td>
                                                <td><?=number_format($row->kredit);?></td>
                                                <td><?=$row->tanggal;?></td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->