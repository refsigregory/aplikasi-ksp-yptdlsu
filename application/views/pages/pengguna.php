
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Pengguna <a href="<?=base_url('home/tambah_pengguna');?>" class="btn btn-primary">Tambah</a>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Nama Pengguna</th>
                                                    <th>Nama Lengkap</th>
                                                    <th>Tipe User</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($pengguna != ""): foreach($pengguna as $row):?>
                                                <td><?=$no;?></td>
                                                <td><?=$row->username;?></td>
                                                <td><?=$row->nama;?></td>
                                                <td><?=$row->tipe_user;?></td>
                                                <td>
                                                        <a href="<?=base_url('home/ubah_pengguna?edit='.$row->id_user);?>">
                                                        <button class="btn btn-sm btn-warning" title="Ubah">Ubah</button>
                                                        </a>
                                                        
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->

<?php
if(isset($_GET['view']))
{
    //$this->load->view('modal_detail_anggota');
}


?>