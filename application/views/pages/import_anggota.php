
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>

                            <?=form_open_multipart(base_url('home/importAnggota'));?>
                            <div class="form-group">
                                Pilih File (format .xls): 
	                            <input class="form-controls" name="filepegawai" type="file" required="required"> 
                            </div>
                            <div class="form-group">
	                            <input class="btn btn-primary" name="upload" type="submit" value="Import">
                            </div>
                        </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->