     <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header">
                            <?=$title;?>
                                    <?php if($this->session->userdata("type") == "anggota"):?>
                                        <a href="<?=base_url('home/ubah_anggota?edit='.$this->session->userdata('id'));?>" class="btn btn-primary">Ubah</a>
                                    <?php endif;?>
                            </h1>
                            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                <div class="form-group">
                    <label>Nama Lengkap :</label>
                    <?=$data->nama;?>
                </div>
                <div class="form-group">
                    <label>Nama Panggilan :</label>
                    <?=$data->nama_panggilan;?>
                </div>
                <div class="form-group">
                    <label>Nomor Identitas :</label>
                    <?=$data->nomor_identitas;?>
                </div>
                <div class="form-group">
                    <label>Email :</label>
                    <?=$data->email;?>
                </div>
                <div class="form-group">
                    <label>Telepon :</label>
                    <?=$data->telepon;?>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin :</label>
                    <?=$data->jenis_kelamin;?>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir :</label>
                    <?=$data->tempat_lahir;?>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir :</label>
                    <?=$data->tanggal_lahir;?>
                </div>
                <div class="form-group">
                    <label>Alamat :</label>
                    <?=$data->alamat;?>
                </div>
                <div class="form-group">
                    <label>Tahun Menetap :</label>
                    <?=$data->tahun_menetap;?>
                </div>
                <div class="form-group">
                    <label>Lingkungan :</label>
                    <?=$data->lingkungan;?>
                </div>
                <div class="form-group">
                    <label>Kelurahan :</label>
                    <?=$data->kelurahan;?>
                </div>
                <div class="form-group">
                    <label>Kecamatan :</label>
                    <?=$data->kecamatan;?>
                </div>
                <div class="form-group">
                    <label>Kota :</label>
                    <?=$data->kota;?>
                </div>
                <div class="form-group">
                    <label>Status Alamat :</label>
                    <?=$data->status_alamat;?>
                </div>
                <div class="form-group">
                    <label>Pekerjaan :</label>
                    <?=$data->pekerjaan;?>
                </div>
                <div class="form-group">
                    <label>Alamat Tempat Kerja :</label>
                    <?=$data->alamat_tempat_kerja;?>
                </div>
                <div class="form-group">
                    <label>Tahun Masuk :</label>
                    <?=$data->tahun_masuk_kerja;?>
                </div>
                <div class="form-group">
                    <label>Penghasilan per bulan :</label>
                    <?=$data->penghasilan_perbulan;?>
                </div>
                <div class="form-group">
                    <label>Status Pernikahan :</label>
                    <?=$data->status_pernikahan;?>
                </div>
                <div class="form-group">
                    <label>Nama Pasangan:</label>
                    <?=$data->nama_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Telepon Pasangan :</label>
                    <?=$data->telepon_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Penghasilan Pasangan :</label>
                    <?=$data->penghasilan_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Pekerjaan Pasangan :</label>
                    <?=$data->pekerjaan_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Tahun Masuk Kerja Pasangan :</label>
                    <?=$data->tahun_masuk_kerja_pasangan;?>
                </div>
                <?php
                  $berkas = $this->anggota_model->getAllBerkas();
                  foreach ($berkas as $row):
                    $id_user = $data->id_user;
                    
                    $berkas_anggota = $this->anggota_model->getBerkasAnggota($row->id_berkas, $id_user);
                ?>
                  <div class="form-group">
                      <label><?=$row->nama_berkas;?>:</label>
                      <?php  if($berkas_anggota != ""): ?>
                        <a href="<?=base_url();?>uploads/<?=$berkas_anggota[0]->nama_file;?>"><span class="label label-success">Lihat File</span></a>
                      <?php else:?>
                        <span class="label label-danger">Tidak ada</span>
                      <?php endif;?>
                  </div>
                <?php
                  endforeach;
                ?>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->