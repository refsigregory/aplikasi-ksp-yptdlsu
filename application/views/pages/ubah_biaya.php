            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>
                            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                            <?=form_open_multipart(base_url('home/ubahBiaya'));?>

                <input type="hidden" name="kode_biaya" value="<?=$edit[0]->kode_biaya;?>" placeholder="">
                
                <div class="form-group">
                    <label>Nama Biaya</label>
                    <input type="text" class="form-control" name="nama_biaya" value="<?=$edit[0]->nama_biaya;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Jumlah Biaya</label>
                    <input type="text" class="form-control" name="jumlah_biaya" value="<?=$edit[0]->jumlah_biaya;?>" placeholder="">
                </div>

                <div class="form-group">
                    <label>Tipe Biaya</label>
                    <select class="form-control" name="tipe_biaya" id="">
                    <option <?=($edit[0]->tipe_biaya == 'nominal') ? 'selected="selected"' : '';?>>nominal</option>
                    <option <?=($edit[0]->tipe_biaya == 'persen') ? 'selected="selected"' : '';?>>persen</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary btn-block">Ubah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->