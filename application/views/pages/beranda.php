
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$page_title;?></h1>
            </div>
            <div class="col-lg-12">
            <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
            <?php if($notif != ""): foreach($notif as $row):?>
                <div class="alert alert-info"><?=$row->pesan;?></div>
            <?php endforeach; endif;?>
            <?php if($this->session->userdata("type") == "anggota"): if($this->anggota_model->getByID($this->session->userdata("id"))[0]->status_anggota == "nonaktif"):?>
                <div class="alert alert-warning">Selamat Datang di <?=$this->config->item("site_name");?>, Lengkapi data diri anda untuk menjadi anggota aktif.</div>
            <?php endif; endif;?> 
            <?php if($this->session->userdata("type") == "anggota"): if($this->anggota_model->getByID($this->session->userdata("id"))[0]->status_anggota == "menunggu"):?>
                <div class="alert alert-warning">Formulir Data diri dan Berkas anda sedang diverifikasi Pengurus.</div>
            <?php endif; endif;?> 
            </div>
            </div>
            <?php if($this->session->userdata("type") == "admin" || $this->session->userdata("type") == "pengurus"):?>
            <div class="row">

            
                        <div class="col-lg-3 col-md-6">
                            <a href="<?=base_url('home/simpanan');?>">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-money fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Rp. <?=number_format($this->transaksi_model->getSumSimpananValid());?></div>
                                            <div>Simpanan</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <a href="<?=base_url('home/pinjaman');?>">
                            <div class="panel panel-green">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-money fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Rp. <?=number_format($this->transaksi_model->getSumPinjamanValid());?></div>
                                            <div>Pinjaman</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <a href="<?=base_url('home/transaksi');?>">
                            <div class="panel panel-yellow">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-money fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge">Rp. <?=number_format(($this->transaksi_model->getSumSimpananValid()-$this->transaksi_model->getSumPinjamanValid()));?></div>
                                            <div>Total Saldo</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-6">
                        <a href="<?=base_url('home/anggota');?>">
                            <div class="panel panel-red">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-xs-3">
                                            <i class="fa fa-users fa-5x"></i>
                                        </div>
                                        <div class="col-xs-9 text-right">
                                            <div class="huge"><?=$this->anggota_model->getCountAll();?></div>
                                            <div>Total Anggota</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
        <!-- /.row -->
        <?php endif;?>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->