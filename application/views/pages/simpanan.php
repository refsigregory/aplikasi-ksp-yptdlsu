
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
           
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Daftar Simpanan  
                                    <?php if($this->session->userdata("type") == "anggota"):?>
                                    <a href="<?=base_url('home/tambah_simpanan');?>" class="btn btn-primary">Tambah</a>
                                    <?php endif;?>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                <p class="login-box-msg">
                <?php
                    if (!empty($this->session->flashdata('msg'))):
                        $msg = $this->session->flashdata('msg');
                ?>
                <?php if($msg['type'] == 'success'): ?>
                    <div class="alert alert-success"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'warning'): ?>
                    <div class="alert alert-warning"><?=$msg['message'];?></div>
                <?php elseif ($msg['type'] == 'error'): ?>
                    <div class="alert alert-danger"><?=$msg['message'];?></div>
                <?php else: ?>
                    <div class="alert alert-info"><?=$msg['message'];?></div>
                <?php endif; ?>
                <?php endif; ?>
            </p>
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No Transaksi</th>
                                                    <?php if($this->session->userdata("type") == "pengurus"):?>
                                                    <th>Nama Anggota</th>
                                                    <?php endif;?>  
                                                    <th>Jenis Simpanan</th>
                                                    <th>Jumlah Simpanan</th>
                                                    <th>Tanggal</th>
                                                    <th>Bukti Pembayaran</th>
                                                    <th>Status</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($simpanan != ""): foreach($simpanan as $row):?>
                                                <td><?=$no;?></td>
                                                <?php if($this->session->userdata("type") == "pengurus"):?>
                                                    <td><?=$this->anggota_model->getByID($row->id_anggota)[0]->nama;?></td>
                                                <?php endif;?> 
                                                <td><?=$row->jenis_simpanan;?></td>
                                                <td><?=number_format($row->jumlah_simpanan);?></td>
                                                <td><?=$row->tanggal;?></td>
                                                <td>
                                                    <?php if($row->bukti_pembayaran != ""):?>
                                                        <a href="<?=base_url('uploads/'.$row->bukti_pembayaran);?>" target="_blank"><span class="label label-success">Lihat</span></a>
                                                    <?php else:?>
                                                        <span class="label label-danger">Tidak ada</span>
                                                    <?php endif;?>
                                                </td>
                                                <td><?=$row->status_simpanan;?></td>
                                                <td>
                                                    <?php if($this->session->userdata("type") == "pengurus"):?>
                                                    <a href="<?=base_url('home/konfirmasiSimpanan?id='.$row->id_simpanan .'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-success" title="Konfirmasi Pembayaran">Konfirmasi</button>
                                                    </a>
                                                    
                                                    
                                                    <a href="<?=base_url('home/tolakSimpanan?id='.$row->id_simpanan .'&id_user='.$row->id_anggota);?>">
                                                    <button class="btn btn-sm btn-danger" title="Tolak"><i class="fa fa-window-pencil"></i>Tolak</button>
                                                    </a>
                                                    <?php endif;?>    
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->