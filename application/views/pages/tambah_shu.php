
            <!-- Page Content -->
            <div id="page-wrapper">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-12">
                            <h1 class="page-header"><?=$title;?></h1>

                            <?=form_open_multipart(base_url('home/tambahSHU'));?>
                <div class="form-group">
                    <label>Tahun</label>
                    <input type="text" class="form-control" name="tahun" placeholder="<?=date("Y");?>">
                </div>

                <div class="form-group">
                    <label>Sisa Hasil Usaha</label>
                    <input type="text" class="form-control" name="sisa_usaha" value="<?=($this->transaksi_model->getSumSimpananValid()-$this->transaksi_model->getSumPinjamanValid());?>">
                </div>


                <button type="submit" class="btn btn-primary btn-block">Tambah</button>
            </form>

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.container-fluid -->
            </div>
            <!-- /#page-wrapper -->