
<!-- Page Content -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header"><?=$title;?></h1>
            </div>
            <!-- /.col-lg-12 -->
            <!-- /.row -->
            <div class="row">
                        <div class="col-lg-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Sisa Hasil Usaha
                                    <?php if($this->session->userdata("type") == "admin"):?>
                                    <a href="<?=base_url('home/tambah_shu');?>" class="btn btn-primary">Tambah SHU</a>
                                    <?php endif;?>
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                            <thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Tahun</th>
                                                    <th>Total SHU</th>
                                                    <th>Aksi</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            <?php $no = 1; if($shu != ""): foreach($shu as $row):?>
                                                <td><?=$no;?></td>
                                                <td><?=$row->tahun;?></td>
                                                <td><?=number_format($row->sisa_usaha);?></td>
                                                <td>
                                                    <a href="<?=base_url('home/detail_sisa_hasil_usaha/?shu='.$row->id_sisa_hasil_usaha);?>" class="btn btn-info"> Lihat Detail</a>
                                                </td>
                                                </tr>
                                                <?php $no++; endforeach; endif;?>

                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.panel-body -->
                            </div>
                            <!-- /.panel -->
                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->