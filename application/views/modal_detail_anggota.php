
  <?php
    $data = $this->anggota_model->getByID($this->input->get('view'))[0];
  ?>
  <div class="modal fade" id="show-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Data Anggota</h4>
      </div>
      
      <div class="modal-body">
      <div class="form-group">
                    <label>Nama Lengkap :</label>
                    <?=$data->nama;?>
                </div>
                <div class="form-group">
                    <label>Nama Panggilan :</label>
                    <?=$data->nama_panggilan;?>
                </div>
                <div class="form-group">
                    <label>Nomor Identitas :</label>
                    <?=$data->nomor_identitas;?>
                </div>
                <div class="form-group">
                    <label>Email :</label>
                    <?=$data->email;?>
                </div>
                <div class="form-group">
                    <label>Telepon :</label>
                    <?=$data->telepon;?>
                </div>
                <div class="form-group">
                    <label>Jenis Kelamin :</label>
                    <?=$data->jenis_kelamin;?>
                </div>
                <div class="form-group">
                    <label>Tempat Lahir :</label>
                    <?=$data->tempat_lahir;?>
                </div>
                <div class="form-group">
                    <label>Tanggal Lahir :</label>
                    <?=$data->tanggal_lahir;?>
                </div>
                <div class="form-group">
                    <label>Alamat :</label>
                    <?=$data->alamat;?>
                </div>
                <div class="form-group">
                    <label>Tahun Menetap :</label>
                    <?=$data->tahun_menetap;?>
                </div>
                <div class="form-group">
                    <label>Lingkungan :</label>
                    <?=$data->lingkungan;?>
                </div>
                <div class="form-group">
                    <label>Kelurahan :</label>
                    <?=$data->kelurahan;?>
                </div>
                <div class="form-group">
                    <label>Kecamatan :</label>
                    <?=$data->kecamatan;?>
                </div>
                <div class="form-group">
                    <label>Kota :</label>
                    <?=$data->kota;?>
                </div>
                <div class="form-group">
                    <label>Status Alamat :</label>
                    <?=$data->status_alamat;?>
                </div>
                <div class="form-group">
                    <label>Pekerjaan :</label>
                    <?=$data->pekerjaan;?>
                </div>
                <div class="form-group">
                    <label>Alamat Tempat Kerja :</label>
                    <?=$data->alamat_tempat_kerja;?>
                </div>
                <div class="form-group">
                    <label>Tahun Masuk :</label>
                    <?=$data->tahun_masuk_kerja;?>
                </div>
                <div class="form-group">
                    <label>Penghasilan per bulan :</label>
                    <?=$data->penghasilan_perbulan;?>
                </div>
                <div class="form-group">
                    <label>Status Pernikahan :</label>
                    <?=$data->status_pernikahan;?>
                </div>
                <div class="form-group">
                    <label>Nama Pasangan:</label>
                    <?=$data->nama_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Telepon Pasangan :</label>
                    <?=$data->telepon_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Penghasilan Pasangan :</label>
                    <?=$data->penghasilan_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Pekerjaan Pasangan :</label>
                    <?=$data->pekerjaan_pasangan;?>
                </div>
                <div class="form-group">
                    <label>Tahun Masuk Kerja Pasangan :</label>
                    <?=$data->tahun_masuk_kerja_pasangan;?>
                </div>
                <?php
                  $berkas = $this->anggota_model->getAllBerkas();
                  foreach ($berkas as $row):
                    $id_user = $this->input->get('view');
                    
                    $berkas_anggota = $this->anggota_model->getBerkasAnggota($row->id_berkas, $id_user);
                ?>
                  <div class="form-group">
                      <label><?=$row->nama_berkas;?>:</label>
                      <?php  if($berkas_anggota != ""): ?>
                        <a href="<?=base_url();?>uploads/<?=$berkas_anggota[0]->nama_file;?>"><span class="label label-success">Lihat File</span></a>
                      <?php else:?>
                        <span class="label label-danger">Tidak ada</span>
                      <?php endif;?>
                  </div>
                <?php
                  endforeach;
                ?>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Tutup</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
