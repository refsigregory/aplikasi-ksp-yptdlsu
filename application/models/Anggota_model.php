<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anggota_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("select * from pengguna where tipe_user = 'anggota'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getCountAll()
    {
        $query=$this->db->query("select count(*) as jumlah from pengguna where tipe_user = 'anggota'");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }

    public function getByID($id)
    {
        $query=$this->db->query("select * from pengguna where id_user = $id");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAllPengguna()
    {
        $query=$this->db->query("select * from pengguna where tipe_user != 'anggota'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAktifAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'aktif'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPindahAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'pindah'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getMeninggalAll()
    {
        $query=$this->db->query("select * from tb_anggota where status_anggota = 'meninggal'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insert($data)
    {
        $query = $this->db->insert("pengguna", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function insertBerkas($data)
    {
        if($this->getBerkasAnggota($data['id_berkas'], $data['id_anggota']) == "") {
            $query = $this->db->insert("berkas_anggota", $data);
            if($query)
            {
                return true;
            } else {
                return false;
            }
        } else {
            $this->db->where("id_berkas", $data['id_berkas']);
            $this->db->where("id_anggota", $data['id_anggota']);
            unset($data['id_berkas']);
            unset($data['id_anggota']);
            $query = $this->db->update("berkas_anggota", $data);
            if($query)
            {
                return true;
            } else {
                return false;
            }
        }
    }

    public function update($id, $data)
    {
        $this->db->where("id_user", $id);
        $query = $this->db->update("pengguna", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function getNotifByUser($id)
    {
        $query=$this->db->query("select * from notifikasi where id_user = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function insertNotif($user, $pesan)
    {
        $query = $this->db->insert("notifikasi", ["id_user" => $user, "pesan" => $pesan, "tanggal"=>date("Y-m-d H:i:s")]);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function getAllMetadata()
    {
        $query=$this->db->query("select * from metadata");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getMetadata($id)
    {
        $query=$this->db->query("select * from metadata where nama_metadata = '$id'");
        if($query->num_rows()>0)
        {
            return $query->row();
        } else {
            return "";
        }
    }

    public function getBerkasAnggota($id_berkas, $id_anggota)
    {
        $query=$this->db->query("select * from berkas_anggota where id_berkas = '$id_berkas' AND id_anggota = '$id_anggota'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAllBiaya()
    {
        $query=$this->db->query("select * from biaya");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getBiaya($id)
    {
        $query=$this->db->query("select * from biaya where kode_biaya = '$id'");
        if($query->num_rows()>0)
        {
            return $query->row();
        } else {
            return "";
        }
    }

    public function getAllBerkas()
    {
        $query=$this->db->query("select * from berkas");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }
}


