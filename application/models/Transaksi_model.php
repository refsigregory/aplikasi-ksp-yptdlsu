<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transaksi_model extends CI_Model
{
    public function getAll()
    {
        $query=$this->db->query("SELECT id_pinjaman as id_transaksi, 'pinjaman' as tipe, id_anggota, '0' as debet, jumlah_pinjaman as kredit, tanggal_pinjaman as tanggal FROM  `pinjaman` where status_pinjaman = 'terverifikasi'OR status_pinjaman = 'diterima' UNION SELECT  id_simpanan as id_transaksi, 'simpanan' as tipe,  id_anggota, jumlah_simpanan as debet, '0' as kredit, tanggal  FROM `simpanan` where status_simpanan = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }


    public function getAllByAnggota($id)
    {
        $query=$this->db->query("SELECT id_pinjaman as id_transaksi, 'pinjaman' as tipe, id_anggota, '0' as debet, jumlah_pinjaman as kredit, tanggal_pinjaman as tanggal FROM  `pinjaman` where id_anggota = '$id' and status_pinjaman = 'terverifikasi'OR status_pinjaman = 'diterima' UNION SELECT  id_simpanan as id_transaksi, 'simpanan' as tipe,  id_anggota, jumlah_simpanan as debet, '0' as kredit, tanggal  FROM `simpanan` where id_anggota = '$id' and status_simpanan = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getAllByAnggotaBulanTahun($id, $bulan, $tahun)
    {
        $query=$this->db->query("SELECT id_pinjaman as id_transaksi, 'pinjaman' as tipe, id_anggota, '0' as debet, jumlah_pinjaman as kredit, tanggal_pinjaman as tanggal FROM  `pinjaman` where id_anggota = '$id' AND month(tanggal_pinjaman) = '$bulan' AND year(tanggal_pinjaman) = '$year' and status_pinjaman = 'terverifikasi' OR status_pinjaman = 'diterima' UNION SELECT  id_simpanan as id_transaksi, 'simpanan' as tipe,  id_anggota, jumlah_simpanan as debet, '0' as kredit, tanggal  FROM `simpanan` where id_anggota = '$id' AND month(tanggal) = '$bulan' AND year(tanggal) = '$tahun'  and status_simpanan = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getSisaHasilUsahaAll()
    {
        $query=$this->db->query("select * from sisa_hasil_usaha");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function checkSHUByIDAndAnggota($id, $anggota)
    {
        $query=$this->db->query("select * from sisa_hasil_usaha_detail where id_sisa_hasil_usaha = '$id' and id_anggota = '$anggota'");
        return $query->num_rows();
    }

    public function getSisaHasilUsahaByID($id)
    {
        $query=$this->db->query("select * from sisa_hasil_usaha where id_sisa_hasil_usaha = '$id'");
        return $query->num_rows();
    }

    public function getDetailSisaHasilUsaha($id)
    {
        $query=$this->db->query("select * from sisa_hasil_usaha_detail where id_sisa_hasil_usaha = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }


    public function getBiayaAll()
    {
        $query=$this->db->query("select * from biaya");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getSimpananByAnggota($id)
    {
        $query=$this->db->query("select * from simpanan where id_anggota = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getSimpananAll()
    {
        $query=$this->db->query("select * from simpanan");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getSumSimpananAll()
    {
        $query=$this->db->query("select sum(jumlah_simpanan) as jumlah from simpanan");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }

    public function getSumSimpananValid()
    {
        $query=$this->db->query("select sum(jumlah_simpanan) as jumlah from simpanan where status_simpanan = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }

    public function checkPinjamanTerlambatByID($id)
    {
        $query=$this->db->query("select * from pinjaman where tanggal_pengembalian < now() and id_pinjaman = '$id'");
        if($query->num_rows()>0)
        {
            return $query->row();
        } else {
            return "";
        }
    }

    

    public function getPinjamanAll()
    {
        $query=$this->db->query("select * from pinjaman");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPinjamanByAnggota($id)
    {
        $query=$this->db->query("select * from pinjaman where id_anggota = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPinjamanByID($id)
    {
        $query=$this->db->query("select * from pinjaman where id_pinjaman = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getPengembalianPinjamanByPinjaman($id)
    {
        $query=$this->db->query("select * from pengembalian_pinjaman where id_pinjaman = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getSumPinjamanValid()
    {
        $query=$this->db->query("select sum(jumlah_pinjaman) as jumlah from pinjaman where status_pinjaman = 'terverifikasi'");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah;
        } else {
            return 0;
        }
    }

    public function getMetadataAll()
    {
        $query=$this->db->query("select * from metadata");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getMetadata($id)
    {
        $query=$this->db->query("select * from metadata where nama_metadata = '$id'");
        if($query->num_rows()>0)
        {
            return $query->row()->metadata;
        } else {
            return "";
        }
    }

    public function getMetadataByID($id)
    {
        $query=$this->db->query("select * from metadata where nama_metadata = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getBiayaByID($id)
    {
        $query=$this->db->query("select * from biaya where kode_biaya = '$id'");
        if($query->num_rows()>0)
        {
            return $query->result();
        } else {
            return "";
        }
    }

    public function getBiaya($id)
    {
        $query=$this->db->query("select * from biaya where kode_biaya = '$id'");
        if($query->num_rows()>0)
        {
            return $query->row()->jumlah_biaya;
        } else {
            return 0;
        }
    }

    public function insert($data)
    {
        $query = $this->db->insert("tb_anggota", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function insertSimpanan($data)
    {
        $query = $this->db->insert("simpanan", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function insertPinjaman($data)
    {
        $query = $this->db->insert("pinjaman", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }


    public function insertCicilan($data)
    {
        $query = $this->db->insert("pengembalian_pinjaman", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function insertSHU($data)
    {
        $query = $this->db->insert("sisa_hasil_usaha", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateSHU($data, $id)
    {
        $this->db-where("id_sisa_hasil_usaha", $id);
        $query = $this->db->insert("sisa_hasil_usaha", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function insertDSHU($data)
    {
        $query = $this->db->insert("sisa_hasil_usaha_detail", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateDSHU($data, $id)
    {
        $this->db->where("id_shu_detail", $id);
        $query = $this->db->update("sisa_hasil_usaha_detail", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }
    
    public function update($id, $data)
    {
        $this->db->where("id_anggota", $id);
        $query = $this->db->update("tb_anggota", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateBiaya($id, $data)
    {
        $this->db->where("kode_biaya", $id);
        $query = $this->db->update("biaya", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateMetadata($id, $data)
    {
        $this->db->where("nama_metadata", $id);
        $query = $this->db->update("metadata", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updatePinjaman($id, $data)
    {
        $this->db->where("id_pinjaman", $id);
        $query = $this->db->update("pinjaman", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateCicilan($id, $data)
    {
        $this->db->where("id_pengembalian_pinjaman", $id);
        $query = $this->db->update("pengembalian_pinjaman", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }

    public function updateSimpanan($id, $data)
    {
        $this->db->where("id_simpanan", $id);
        $query = $this->db->update("simpanan", $data);
        if($query)
        {
            return true;
        } else {
            return false;
        }
    }
}
