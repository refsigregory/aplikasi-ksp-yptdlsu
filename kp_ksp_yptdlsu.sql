-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 18, 2019 at 09:03 AM
-- Server version: 10.1.34-MariaDB-0ubuntu0.18.04.1
-- PHP Version: 7.2.19-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kp_ksp_yptdlsu`
--

-- --------------------------------------------------------

--
-- Table structure for table `berkas`
--

CREATE TABLE `berkas` (
  `id_berkas` int(11) NOT NULL,
  `nama_berkas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berkas`
--

INSERT INTO `berkas` (`id_berkas`, `nama_berkas`) VALUES
(1, 'KTP'),
(2, 'Kartu Pegawai'),
(3, 'Kartu Keluarga');

-- --------------------------------------------------------

--
-- Table structure for table `berkas_anggota`
--

CREATE TABLE `berkas_anggota` (
  `id_berkas_anggota` int(11) NOT NULL,
  `id_berkas` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `nama_file` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `berkas_anggota`
--

INSERT INTO `berkas_anggota` (`id_berkas_anggota`, `id_berkas`, `id_anggota`, `nama_file`) VALUES
(2, 1, 14, ''),
(3, 2, 14, ''),
(4, 1, 15, ''),
(5, 2, 15, ''),
(6, 3, 15, ''),
(9, 1, 2, '478ebb452c4aa2170396971b08484ee4.jpg'),
(10, 2, 2, '114d36fbcb089fc786cc5153b7a57b35.jpg'),
(11, 1, 16, '87ad4d52b3eb8888a524b4c59b7a34d8.jpg'),
(12, 1, 32, '7286784bce839719391b5683d2b9c190.jpg'),
(13, 2, 32, 'd7fd10f001ff5cc6ae5f539d951b1f91.jpg'),
(14, 3, 32, '7e46f07fb37c316caba68b58f08e4e6b.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `biaya`
--

CREATE TABLE `biaya` (
  `kode_biaya` varchar(50) NOT NULL,
  `nama_biaya` varchar(50) NOT NULL,
  `jumlah_biaya` double NOT NULL,
  `tipe_biaya` set('persen','nominal') NOT NULL DEFAULT 'persen'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `biaya`
--

INSERT INTO `biaya` (`kode_biaya`, `nama_biaya`, `jumlah_biaya`, `tipe_biaya`) VALUES
('bunga_bulanan', 'Bunga Bulanan', 0.5, 'nominal'),
('bunga_pinjaman', 'Bunga Pinjaman', 0.5, 'nominal'),
('denda_pinjaman', 'Denda Pinjaman', 0.2, 'persen'),
('potongan_tahunan', 'Potongan Tahunan', 0.2, 'nominal');

-- --------------------------------------------------------

--
-- Table structure for table `metadata`
--

CREATE TABLE `metadata` (
  `nama_metadata` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL,
  `metadata` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `metadata`
--

INSERT INTO `metadata` (`nama_metadata`, `keterangan`, `metadata`) VALUES
('batas_simpanan_wajib', 'Tanggal Batas Waktu Simpanan', '12');

-- --------------------------------------------------------

--
-- Table structure for table `notifikasi`
--

CREATE TABLE `notifikasi` (
  `id_notifikasi` int(50) NOT NULL,
  `id_user` varchar(50) NOT NULL,
  `pesan` text NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notifikasi`
--

INSERT INTO `notifikasi` (`id_notifikasi`, `id_user`, `pesan`, `tanggal`) VALUES
(1, '15', 'Pendaftaran Anda Telah Diterima', '2019-12-13 08:41:31'),
(2, '32', 'Pendaftaran Anda Telah Diterima', '2019-12-18 09:00:46');

-- --------------------------------------------------------

--
-- Table structure for table `pengembalian_pinjaman`
--

CREATE TABLE `pengembalian_pinjaman` (
  `id_pengembalian_pinjaman` int(11) NOT NULL,
  `id_pinjaman` int(11) NOT NULL,
  `nominal` double NOT NULL DEFAULT '0',
  `bukti_pembayaran` varchar(100) NOT NULL,
  `bukti_pembayaran_denda` varchar(100) DEFAULT NULL,
  `tanggal` date NOT NULL,
  `status_pengembalian` set('menunggu','diterima','ditolak') NOT NULL DEFAULT 'menunggu'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tipe_user` set('admin','pengurus','anggota') NOT NULL DEFAULT 'anggota',
  `nama` varchar(50) DEFAULT NULL,
  `nama_panggilan` varchar(50) DEFAULT NULL,
  `nomor_identitas` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `jenis_kelamin` set('Laki-laki','Perempuan') DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL,
  `tahun_menetap` year(4) DEFAULT NULL,
  `lingkungan` varchar(50) DEFAULT NULL,
  `kelurahan` varchar(50) DEFAULT NULL,
  `kecamatan` varchar(50) DEFAULT NULL,
  `kota` varchar(50) DEFAULT NULL,
  `status_alamat` varchar(50) DEFAULT NULL,
  `pekerjaan` varchar(50) DEFAULT NULL,
  `alamat_tempat_kerja` varchar(50) DEFAULT NULL,
  `tahun_masuk_kerja` year(4) DEFAULT NULL,
  `penghasilan_perbulan` varchar(50) DEFAULT NULL,
  `status_pernikahan` varchar(50) DEFAULT NULL,
  `nama_pasangan` varchar(50) DEFAULT NULL,
  `telepon_pasangan` varchar(50) DEFAULT NULL,
  `penghasilan_pasangan` varchar(50) DEFAULT NULL,
  `pekerjaan_pasangan` varchar(50) DEFAULT NULL,
  `tahun_masuk_kerja_pasangan` year(4) DEFAULT NULL,
  `id_berkas` int(11) DEFAULT NULL,
  `tipe_anggota` set('Karyawan','Mahasiswa') DEFAULT 'Karyawan',
  `status_anggota` set('aktif','nonaktif','menunggu') DEFAULT 'nonaktif',
  `verifikasi` set('sudah','belum') DEFAULT 'belum'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`id_user`, `username`, `password`, `tipe_user`, `nama`, `nama_panggilan`, `nomor_identitas`, `email`, `telepon`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `alamat`, `tahun_menetap`, `lingkungan`, `kelurahan`, `kecamatan`, `kota`, `status_alamat`, `pekerjaan`, `alamat_tempat_kerja`, `tahun_masuk_kerja`, `penghasilan_perbulan`, `status_pernikahan`, `nama_pasangan`, `telepon_pasangan`, `penghasilan_pasangan`, `pekerjaan_pasangan`, `tahun_masuk_kerja_pasangan`, `id_berkas`, `tipe_anggota`, `status_anggota`, `verifikasi`) VALUES
(1, 'admin', 'admin', 'admin', 'Admin', '', '', '', '', '', '', '0000-00-00', '', NULL, '', '', '', '', '', NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0000, 0, 'Karyawan', 'aktif', 'sudah'),
(2, 'alexdoe', 'alexdoe', 'anggota', 'Alex Doe', '', '', 'alexy@doe', '123', 'Laki-laki', '', '0000-00-00', 'Huuuu', NULL, '', '', '', '', 'Rumah Sendiri', NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0000, 0, 'Karyawan', 'aktif', 'sudah'),
(3, 'pengurus', 'pengurus', 'pengurus', 'Pengurus', '', '', 'test@test', 'Test', 'Laki-laki', '', '0000-00-00', 'asdsad', NULL, '', '', '', '', 'Rumah Sendiri', NULL, NULL, NULL, NULL, NULL, '', '', '', '', 0000, 0, 'Karyawan', 'aktif', 'sudah'),
(15, 'reref', 'reref', 'anggota', 'Reref', 'ref', '12345', 'reref@mail.com', '08123456789', 'Laki-laki', 'Manado', '2000-10-11', 'Manado', 2018, 'V', 'Asd', 'Ad', 'Ad', 's/d 1 juta', 'asd', 'Asd', 2001, NULL, 'Belum Menikah', NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'aktif', 'belum'),
(16, '123', '123', 'anggota', '123', '123', '123', '123@123', '123', 'Laki-laki', '123', '0000-00-00', '123', 0000, '123', '123', '123', '123', 's/d 1 juta', '123', '123', 0000, NULL, 'Belum Menikah', NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'menunggu', 'belum'),
(17, '123', '123', 'anggota', '123123', '123', '123', '1231@123', '123', 'Laki-laki', '123', '1980-12-12', '123', 0000, '123', '123123', '123', '123', '1 s/d 2 juta', '1231', '123', 0000, NULL, 'Belum Menikah', NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'nonaktif', 'belum'),
(32, 'akuu', 'akuu', 'anggota', 'AKU', 'Aku', '123', 'ini@akuu', '1234', 'Laki-laki', '123', '2000-10-11', '123', 0000, '123', '123', '123', '123', 's/d 1 juta', '123', '123', 0000, NULL, 'Belum Menikah', NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'aktif', 'belum'),
(33, 'kamuu', 'kamuu', 'anggota', 'Kamuu', NULL, NULL, NULL, '124', NULL, NULL, NULL, 'kamupealamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'nonaktif', 'belum'),
(34, 'diaa', 'diaa', 'anggota', 'Diaa', NULL, NULL, NULL, '125', NULL, NULL, NULL, 'diapealamat', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Karyawan', 'nonaktif', 'belum');

-- --------------------------------------------------------

--
-- Table structure for table `pinjaman`
--

CREATE TABLE `pinjaman` (
  `id_pinjaman` int(11) NOT NULL,
  `cara_pembayaran` varchar(10) NOT NULL DEFAULT 'Bank',
  `nomor_rekening` varchar(50) NOT NULL,
  `nama_bank` varchar(50) NOT NULL,
  `nama_rekening` varchar(50) NOT NULL,
  `jumlah_pinjaman` double NOT NULL,
  `tujuan_pinjaman` varchar(50) NOT NULL,
  `bunga_pinjaman` double NOT NULL,
  `tanggal_pinjaman` date NOT NULL,
  `tanggal_pengembalian` date NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `status_pinjaman` set('menunggu','terverifikasi','diterima','ditolak','lunas','terlambat') NOT NULL DEFAULT 'menunggu'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simpanan`
--

CREATE TABLE `simpanan` (
  `id_simpanan` int(11) NOT NULL,
  `jenis_simpanan` varchar(100) NOT NULL,
  `jumlah_simpanan` double NOT NULL DEFAULT '0',
  `id_anggota` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `bukti_pembayaran` varchar(100) NOT NULL,
  `status_simpanan` set('menunggu','terverifikasi','ditolak') NOT NULL DEFAULT 'menunggu'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sisa_hasil_usaha`
--

CREATE TABLE `sisa_hasil_usaha` (
  `id_sisa_hasil_usaha` int(11) NOT NULL,
  `sisa_usaha` double NOT NULL,
  `tahun` year(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sisa_hasil_usaha_detail`
--

CREATE TABLE `sisa_hasil_usaha_detail` (
  `id_shu_detail` int(11) NOT NULL,
  `id_sisa_hasil_usaha` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `persen_pembagian` double NOT NULL,
  `total` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berkas`
--
ALTER TABLE `berkas`
  ADD PRIMARY KEY (`id_berkas`);

--
-- Indexes for table `berkas_anggota`
--
ALTER TABLE `berkas_anggota`
  ADD PRIMARY KEY (`id_berkas_anggota`),
  ADD KEY `id_anggota` (`id_anggota`),
  ADD KEY `id_berkas` (`id_berkas`);

--
-- Indexes for table `biaya`
--
ALTER TABLE `biaya`
  ADD PRIMARY KEY (`kode_biaya`);

--
-- Indexes for table `metadata`
--
ALTER TABLE `metadata`
  ADD PRIMARY KEY (`nama_metadata`);

--
-- Indexes for table `notifikasi`
--
ALTER TABLE `notifikasi`
  ADD PRIMARY KEY (`id_notifikasi`),
  ADD KEY `id_users` (`id_user`);

--
-- Indexes for table `pengembalian_pinjaman`
--
ALTER TABLE `pengembalian_pinjaman`
  ADD PRIMARY KEY (`id_pengembalian_pinjaman`),
  ADD KEY `id_pinjaman` (`id_pinjaman`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`id_user`),
  ADD KEY `id_berkas` (`id_berkas`);

--
-- Indexes for table `pinjaman`
--
ALTER TABLE `pinjaman`
  ADD PRIMARY KEY (`id_pinjaman`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `simpanan`
--
ALTER TABLE `simpanan`
  ADD PRIMARY KEY (`id_simpanan`),
  ADD KEY `id_anggota` (`id_anggota`);

--
-- Indexes for table `sisa_hasil_usaha`
--
ALTER TABLE `sisa_hasil_usaha`
  ADD PRIMARY KEY (`id_sisa_hasil_usaha`);

--
-- Indexes for table `sisa_hasil_usaha_detail`
--
ALTER TABLE `sisa_hasil_usaha_detail`
  ADD PRIMARY KEY (`id_shu_detail`),
  ADD KEY `id_sisa_hasil_usaha` (`id_sisa_hasil_usaha`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berkas`
--
ALTER TABLE `berkas`
  MODIFY `id_berkas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `berkas_anggota`
--
ALTER TABLE `berkas_anggota`
  MODIFY `id_berkas_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `notifikasi`
--
ALTER TABLE `notifikasi`
  MODIFY `id_notifikasi` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pengembalian_pinjaman`
--
ALTER TABLE `pengembalian_pinjaman`
  MODIFY `id_pengembalian_pinjaman` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pengguna`
--
ALTER TABLE `pengguna`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `pinjaman`
--
ALTER TABLE `pinjaman`
  MODIFY `id_pinjaman` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `simpanan`
--
ALTER TABLE `simpanan`
  MODIFY `id_simpanan` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sisa_hasil_usaha`
--
ALTER TABLE `sisa_hasil_usaha`
  MODIFY `id_sisa_hasil_usaha` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sisa_hasil_usaha_detail`
--
ALTER TABLE `sisa_hasil_usaha_detail`
  MODIFY `id_shu_detail` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
